# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170214190520) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "driver_rides", force: true do |t|
    t.date     "date"
    t.integer  "free_places", default: 52
    t.integer  "ride_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "driver_rides", ["ride_id"], name: "index_driver_rides_on_ride_id", using: :btree
  add_index "driver_rides", ["user_id"], name: "index_driver_rides_on_user_id", using: :btree

  create_table "order_of_stops", force: true do |t|
    t.integer  "order"
    t.integer  "route_id"
    t.integer  "stop_id"
    t.integer  "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "order_of_stops", ["route_id"], name: "index_order_of_stops_on_route_id", using: :btree
  add_index "order_of_stops", ["stop_id"], name: "index_order_of_stops_on_stop_id", using: :btree

  create_table "refuelings", force: true do |t|
    t.decimal  "capacity"
    t.decimal  "price"
    t.date     "date"
    t.integer  "vehicle_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "refuelings", ["vehicle_id"], name: "index_refuelings_on_vehicle_id", using: :btree

  create_table "reservations", force: true do |t|
    t.boolean  "payment_status", default: false
    t.integer  "user_id"
    t.integer  "driver_ride_id"
    t.integer  "stops_price_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "reservations", ["driver_ride_id"], name: "index_reservations_on_driver_ride_id", using: :btree
  add_index "reservations", ["stops_price_id"], name: "index_reservations_on_stops_price_id", using: :btree
  add_index "reservations", ["user_id"], name: "index_reservations_on_user_id", using: :btree

  create_table "reward_statuses", force: true do |t|
    t.integer  "status",     default: 1
    t.integer  "reward_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "reward_statuses", ["reward_id"], name: "index_reward_statuses_on_reward_id", using: :btree
  add_index "reward_statuses", ["user_id"], name: "index_reward_statuses_on_user_id", using: :btree

  create_table "rewards", force: true do |t|
    t.integer  "price"
    t.text     "description"
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  create_table "rides", force: true do |t|
    t.time     "startTime"
    t.integer  "route_id"
    t.integer  "vehicle_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rides", ["route_id"], name: "index_rides_on_route_id", using: :btree
  add_index "rides", ["vehicle_id"], name: "index_rides_on_vehicle_id", using: :btree

  create_table "rides_week_days", id: false, force: true do |t|
    t.integer "ride_id"
    t.integer "week_day_id"
  end

  add_index "rides_week_days", ["ride_id"], name: "index_rides_week_days_on_ride_id", using: :btree
  add_index "rides_week_days", ["week_day_id"], name: "index_rides_week_days_on_week_day_id", using: :btree

  create_table "routes", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "secretary_hours", force: true do |t|
    t.datetime "start"
    t.datetime "end"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "secretary_hours", ["user_id"], name: "index_secretary_hours_on_user_id", using: :btree

  create_table "secretary_hours_week_days", id: false, force: true do |t|
    t.integer "secretary_hours_id"
    t.integer "week_day_id"
  end

  add_index "secretary_hours_week_days", ["secretary_hours_id"], name: "index_secretary_hours_week_days_on_secretary_hours_id", using: :btree
  add_index "secretary_hours_week_days", ["week_day_id"], name: "index_secretary_hours_week_days_on_week_day_id", using: :btree

  create_table "stops", force: true do |t|
    t.string   "city"
    t.string   "place"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stops_prices", force: true do |t|
    t.decimal  "price"
    t.integer  "startStop_id"
    t.integer  "endStop_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "firstName",                              null: false
    t.string   "lastName",                               null: false
    t.date     "birthDate",                              null: false
    t.integer  "points",                 default: 0
    t.boolean  "owner",                  default: false
    t.boolean  "driver",                 default: false
    t.boolean  "secretary",              default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "vehicles", force: true do |t|
    t.string   "brand"
    t.string   "model"
    t.text     "description"
    t.decimal  "mileage"
    t.integer  "places"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "week_days", force: true do |t|
    t.string   "day"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
