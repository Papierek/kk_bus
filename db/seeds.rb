# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


# users
User.create(email: 'admin@admin.pl', password: "qweasdzxc", firstName: "Jan", lastName:"Administracyjny", birthDate: '1992-09-24', owner: true)

User.create(email: 'tomasz@rajdowiec.pl', password: "qweasdzxc", firstName: "Tomasz", lastName:"Rajdowiec", birthDate: '1992-09-24', driver: true)
User.create(email: 'kazimierz@rajdowiec.pl', password: "qweasdzxc", firstName: "Kazimierz", lastName:"Rajdowiec", birthDate: '1992-09-24', driver: true)
User.create(email: 'mirosław@szybki.pl', password: "qweasdzxc", firstName: "Mirosław", lastName:"Szybki", birthDate: '1992-09-24', driver: true)
User.create(email: 'jan@doswiadczony.pl', password: "qweasdzxc", firstName: "Jan", lastName:"Doświadczony", birthDate: '1992-09-24', driver: true)
User.create(email: 'marek@poprawny.pl', password: "qweasdzxc", firstName: "Marek", lastName:"Poprawny", birthDate: '1992-09-24', driver: true)
User.create(email: 'zuzanna@konkretna.pl', password: "qweasdzxc", firstName: "Zuzanna", lastName:"Konkretna", birthDate: '1992-09-24', driver: true)

User.create(email: 'piotr@uprzejmy.pl', password: "qweasdzxc", firstName: "Piotr", lastName:"Uprzejmy", birthDate: '1992-09-24', secretary: true)
User.create(email: 'agnieszka@mila.pl', password: "qweasdzxc", firstName: "Agnieszka", lastName:"Miła", birthDate: '1992-09-24', secretary: true)

# weekdays
WeekDay.create(day: 'Poniedziałek')
WeekDay.create(day: 'Wtorek')
WeekDay.create(day: 'Środa')
WeekDay.create(day: 'Czwartek')
WeekDay.create(day: 'Piątek')
WeekDay.create(day: 'Sobota')
WeekDay.create(day: 'Niedziela')

# stops
Stop.create(city: 'Kraków', place: 'Dworzec główny');
Stop.create(city: 'Katowice', place: 'Plac Andrzeja');
Stop.create(city: 'Chrzanów', place: 'Dworzec PKS');

# vehicles
Vehicle.create(brand: 'Opel', model: 'Astra', description: 'Rodzinny opel 1.4', mileage: '80000');