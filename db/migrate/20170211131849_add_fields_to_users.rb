class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :firstName, :string, null: false
    add_column :users, :lastName, :string, null: false
    add_column :users, :birthDate, :date, null: false
    add_column :users, :points, :integer, :default => 0
    add_column :users, :owner, :boolean, :default => false
    add_column :users, :driver, :boolean, :default => false
    add_column :users, :secretary, :boolean, :default => false
  end
end
