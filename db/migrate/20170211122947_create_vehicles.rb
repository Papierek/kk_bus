class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.string :brand
      t.string :model
      t.text :description
      t.decimal :mileage
      t.integer :places

      t.timestamps
    end
  end
end
