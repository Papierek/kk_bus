class CreateRefuelings < ActiveRecord::Migration
  def change
    create_table :refuelings do |t|
      t.decimal :capacity
      t.decimal :price
      t.date :date
      t.belongs_to :vehicle, index: true

      t.timestamps
    end
  end
end
