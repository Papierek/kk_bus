class CreateOrderOfStops < ActiveRecord::Migration
  def change
    create_table :order_of_stops do |t|
      t.integer :order
      t.belongs_to :route, index: true
      t.belongs_to :stop, index: true
      t.integer :time

      t.timestamps
    end
  end
end
