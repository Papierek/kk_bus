class CreateRewards < ActiveRecord::Migration
  def change
    create_table :rewards do |t|
      t.integer :price
      t.text :description
      t.string :title

      t.timestamps
    end
  end
end
