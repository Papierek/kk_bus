class CreateDriverRides < ActiveRecord::Migration
  def change
    create_table :driver_rides do |t|
      t.date :date
      t.integer :free_places, :default => 52
      t.belongs_to :ride, index: true
      t.belongs_to :user, index: true

      t.timestamps
    end
  end
end
