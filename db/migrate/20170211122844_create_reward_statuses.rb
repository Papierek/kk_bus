class CreateRewardStatuses < ActiveRecord::Migration
  def change
    create_table :reward_statuses do |t|
      t.integer :status, default: 1
      t.belongs_to :reward, index: true
      t.belongs_to :user, index: true

      t.timestamps
    end
  end
end
