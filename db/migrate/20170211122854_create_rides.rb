class CreateRides < ActiveRecord::Migration
  def change
    create_table :rides do |t|
      t.time :startTime

      t.belongs_to :route, index: true
      t.belongs_to :vehicle, index: true

      t.timestamps
    end

    create_table :rides_week_days, id: false do |t|
      t.belongs_to :ride, index: true
      t.belongs_to :week_day, index: true
    end
  end
end
