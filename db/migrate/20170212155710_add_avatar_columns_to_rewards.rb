class AddAvatarColumnsToRewards < ActiveRecord::Migration
  def self.up
    add_attachment :rewards, :avatar
  end

  def self.down
    remove_attachment :rewards, :avatar
  end
end
