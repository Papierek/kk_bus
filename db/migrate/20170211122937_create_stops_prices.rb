class CreateStopsPrices < ActiveRecord::Migration
  def change
    create_table :stops_prices do |t|
      t.decimal :price
      t.integer :startStop_id
      t.integer :endStop_id

      t.timestamps
    end
  end
end
