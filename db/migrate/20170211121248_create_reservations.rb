class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.boolean :payment_status, default: false
      t.references :user, index: true
      t.belongs_to :driver_ride, index: true
      t.belongs_to :stops_price, index: true

      t.timestamps
    end
  end
end
