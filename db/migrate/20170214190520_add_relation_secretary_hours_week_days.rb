class AddRelationSecretaryHoursWeekDays < ActiveRecord::Migration
  def change
    create_table :secretary_hours_week_days, id: false do |t|
      t.belongs_to :secretary_hours, index: true
      t.belongs_to :week_day, index: true
    end
  end
end
