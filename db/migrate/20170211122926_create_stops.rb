class CreateStops < ActiveRecord::Migration
  def change
    create_table :stops do |t|
      t.string :city
      t.string :place

      t.timestamps
    end
  end
end
