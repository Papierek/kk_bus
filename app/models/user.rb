class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :rewards, :through => :reward_statuses
  has_many :reservations
  has_many :secretary_hourses
  has_many :driver_rides

  validates_presence_of :firstName, :lastName

  def friendly_name
    "#{firstName} #{lastName}"
  end
end
