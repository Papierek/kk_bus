class RewardStatus < ActiveRecord::Base
  belongs_to :reward
  belongs_to :user

  def status_name
    case status
      when 1
        "Oczekuje na rozpatrzenie"
      when 2
        "Oczekuje na wysyłkę"
      when 3
        "Wysłano"
      else
        "Błąd"
    end
  end
end
