class StopsPrice < ActiveRecord::Base
  belongs_to :startStop, :class_name => 'Stop', :foreign_key => 'startStop_id'
  belongs_to :endStop, :class_name => 'Stop', :foreign_key => 'endStop_id'

  def friendly_name
    "#{startStop.city_with_place} - #{endStop.city_with_place} - #{price}PLN"
  end
end
