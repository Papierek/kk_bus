class DriverRide < ActiveRecord::Base
  belongs_to :user
  belongs_to :ride
  has_many :reservations

  validates_presence_of :date, :ride_id
end
