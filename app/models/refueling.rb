class Refueling < ActiveRecord::Base
  belongs_to :vehicle

  validates_presence_of :capacity, :price
end
