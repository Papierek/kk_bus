class Vehicle < ActiveRecord::Base
  has_many :rides
  has_many :refuelings

  validates_presence_of :brand, :model

  def friendly_name
    "#{id}: #{brand} - #{model}"
  end
end
