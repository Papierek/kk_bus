class Reservation < ActiveRecord::Base
  belongs_to :user
  belongs_to :driver_ride
  belongs_to :stops_price
  attr_accessor :ride

  def friendly_name
    "#{driver_ride.try(:ride).try(:route).try(:friendly_name)} <strong>#{date} #{driver_ride.try(:ride).startTime.strftime("%k:%M")}</strong>"
  end
end
