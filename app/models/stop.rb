class Stop < ActiveRecord::Base
  has_many :routes, :through => :order_of_stops
  has_many :stops_prices_start, :class_name => 'StopsPrice', :foreign_key => 'startStop_id'
  has_many :stops_prices_end, :class_name => 'StopsPrice', :foreign_key => 'endStop_id'

  validates_presence_of :city, :place

  def city_with_place
    "#{city} - #{place}"
  end
end
