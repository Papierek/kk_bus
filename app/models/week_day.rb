class WeekDay < ActiveRecord::Base
  has_and_belongs_to_many :rides
  has_and_belongs_to_many :secretary_hours

  def friendly_name
    case id
      when 1
        "Pn"
      when 2
        "Wt"
      when 3
        "Śr"
      when 4
        "Cz"
      when 5
        "Pt"
      when 6
        "Sb"
      when 7
        "Nd"
      else
        puts "You gave me #{id} -- I have no idea what to do with that."
    end
  end
end
