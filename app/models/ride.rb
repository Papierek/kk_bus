class Ride < ActiveRecord::Base
  has_many :driver_rides
  belongs_to :vehicle
  belongs_to :route
  has_and_belongs_to_many :week_days

  def friendly_name
    days = ""
    week_days.each do |w|
      days += w.friendly_name + " "
    end
    "#{route.friendly_name} - #{startTime.strftime("%k:%M")} - #{end_time} - #{days}"
  end

  def end_time
    (startTime + route.order_of_stops.order(:order).last.time * 60).strftime("%k:%M")
  end

  def stop_time stop
    if time =  route.order_of_stops.order(:order).find(stop).time
      (startTime + time * 60).strftime("%k:%M")
    else
      (startTime).strftime("%k:%M")
    end
  end
end
