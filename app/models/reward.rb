class Reward < ActiveRecord::Base
  has_many :users, :through => :reward_statuses
  validates_presence_of :title,:description, :price

  has_attached_file :avatar, :styles => { :medium => "300x300>", :small => "100x100#", :large => "700x700>" },
                    :processors => [:cropper],
                    :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator, :attributes => :avatar, :less_than => 1.megabytes

  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h, :processing
  after_update :reprocess_avatar, :if => :cropping?

  def cropping?
    !crop_x.blank? && !crop_y.blank? && !crop_w.blank? && !crop_h.blank?
  end

  def avatar_geometry(style = :original)
    @geometry ||= {}
    @geometry[style] ||= Paperclip::Geometry.from_file(avatar.path(style))
  end

  private
  def reprocess_avatar
    return unless (cropping? && !processing)
    self.processing = true
    avatar.reprocess!
    self.processing = false
  end

end
