class Route < ActiveRecord::Base
  has_many :rides
  has_many :order_of_stops, :dependent => :destroy
  has_many :stops, :through => :order_of_stops

  accepts_nested_attributes_for :order_of_stops, :allow_destroy => true

  def friendly_name
    "#{id}: #{order_of_stops.order(:order).first.try(:stop).try(:city_with_place)} -> #{order_of_stops.order(:order).last.try(:stop).try(:city_with_place)}"
  end
end
