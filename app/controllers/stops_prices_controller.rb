class StopsPricesController < ApplicationController
  before_action :authenticate_user!
  before_action :user_check_owner

  def new
    @stopPrice = StopsPrice.new
    @stops = Stop.all
  end

  def edit
    @stopPrice = StopsPrice.find(params[:id])
    @stops = Stop.all
  end

  def create
    @stopPrice = StopsPrice.new(stops_prices_params)
    if @stopPrice.save
      redirect_to @stopPrice
    else
      flash[:alert] = "Wystąpiły błędy"
      render 'new'
    end
  end

  def update
    @stopPrice = StopsPrice.find(params[:id])

    if @stopPrice.update(stops_prices_params)
      redirect_to @stopPrice
    else
      flash[:alert] = "Wystąpiły błędy"
      render 'edit'
    end
  end

  def destroy
    @stopPrice = StopsPrice.find(params[:id])
    @stopPrice.destroy

    redirect_to stops_prices_path
  end

  def show
    @stopPrice = StopsPrice.find(params[:id])
  end

  def index
    @stopPrice = StopsPrice.all
  end


  private
  def stops_prices_params
    params.require(:stops_price).permit(:price, :startStop_id, :endStop_id)

  end
end
