class VehiclesController < ApplicationController
  before_action :authenticate_user!
  before_action :user_check_owner

  def new
    @vehicle = Vehicle.new
  end

  def edit
    @vehicle = Vehicle.find(params[:id])
  end

  def create
    @vehicle = Vehicle.new(vehicle_params)
    if @vehicle.save
      redirect_to @vehicle
    else
      flash[:alert] = "Wystąpiły błędy"
      render 'new'
    end
  end

  def update
    @vehicle = Vehicle.find(params[:id])

    if @vehicle.update(vehicle_params)
      redirect_to @vehicle
    else
      flash[:alert] = "Wystąpiły błędy"
      render 'edit'
    end
  end

  def destroy
    @vehicle = Vehicle.find(params[:id])
    @vehicle.destroy

    redirect_to vehicles_path
  end

  def index
    @vehicle = Vehicle.all
  end



  def show
    @vehicle = Vehicle.find(params[:id])
  end

  private
  def vehicle_params
    params.require(:vehicle).permit(:brand, :model, :description, :mileage)
  end
end
