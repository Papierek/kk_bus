class RoutesController < ApplicationController
  before_action :authenticate_user!
  before_action :user_check_owner

  def new
    @route = Route.new
    @stops = Stop.all
    2.times do
      order_of_stop = @route.order_of_stops.build
    end
  end

  def edit
    @route = Route.find(params[:id])
    @stops = Stop.all
  end

  def create
    @route = Route.new(route_params)
    if @route.save
      redirect_to @route
    else
      flash[:alert] = "Wystąpiły błędy"
      render 'new'
    end
  end

  def update
    @route = Route.find(params[:id])

    if @route.update(route_params)
      redirect_to @route
    else
      flash[:alert] = "Wystąpiły błędy"
      render 'edit'
    end
  end

  def destroy
    @route = Route.find(params[:id])
    @route.destroy

    redirect_to routes_path
  end

  def show
    @route = Route.find(params[:id])
  end

  def index
    @route = Route.all
  end


  private
  def route_params
    params.require(:route).permit(order_of_stops_attributes: [:id, :route_id, :stop_id, :order, :time, :_destroy])
  end
end
