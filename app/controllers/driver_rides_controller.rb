class DriverRidesController < ApplicationController
  before_action :authenticate_user!
  before_action :user_check

  def user_check
    unless current_user.try(:secretary?) or current_user.try(:driver?) or current_user.try(:owner?)
      flash[:error] = "Nie posiadasz uprawnień do przeglądania tej strony"
      redirect_to root_path
    end
  end

  def new
    @driver_ride = DriverRide.new
    @drivers = User.where(driver: true)
    @rides = Ride.all
  end

  def edit
    @driver_ride = DriverRide.find(params[:id])
    @drivers = User.where(driver: true)
    @rides = Ride.all
  end

  def create
    @driver_ride = DriverRide.new(driver_ride_params)
    @drivers = User.where(driver: true)
    @rides = Ride.all
    if weekday_match?
      if @driver_ride.save
        redirect_to @driver_ride
      else
        flash[:alert] = "Wystąpiły błędy"
        render 'new'
      end
    else
      flash[:alert] = "Podany kurs nie występuje w wybranym dniu"
      render 'new'
    end
  end

  def update
    @driver_ride = DriverRide.find(params[:id])
    @drivers = User.where(driver: true)
    @rides = Ride.all
    if weekday_match?
      if @driver_ride.update(driver_ride_params)
        redirect_to @driver_ride
      else
        flash[:alert] = "Wystąpiły błędy"
        render 'edit'
      end
    else
      flash[:alert] = "Podany kurs nie występuje w wybranym dniu"
      render 'new'
    end
  end

  def destroy
    @driver_ride = DriverRide.find(params[:id])
    @driver_ride.destroy

    redirect_to driver_rides_path
  end

  def show
    @driver_ride = DriverRide.find(params[:id])
  end

  def index
    @driver_ride = DriverRide.all
  end


  private
  def driver_ride_params
    params.require(:driver_ride).permit(:date, :ride_id, :user_id)
  end

  def weekday_match?
    date = Date.new params[:driver_ride]["date(1i)"].to_i, params[:driver_ride]["date(2i)"].to_i, params[:driver_ride]["date(3i)"].to_i
    ride_days = Ride.find(params[:driver_ride][:ride_id]).week_days.pluck(:id)

    if date.wday == 0
      day = 7
    else
      day = date.wday
    end

    ride_days.include?(day)
  end
end
