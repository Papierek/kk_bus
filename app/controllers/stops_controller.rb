class StopsController < ApplicationController
  before_action :authenticate_user!
  before_action :user_check_owner

  def new
    @stop = Stop.new
  end

  def edit
    @stop = Stop.find(params[:id])
  end

  def create
    @stop = Stop.new(stop_params)
    if @stop.save
      redirect_to @stop
    else
      flash[:alert] = "Wystąpiły błędy"
      render 'new'
    end
  end

  def update
    @stop = Stop.find(params[:id])

    if @stop.update(stop_params)
      redirect_to @stop
    else
      flash[:alert] = "Wystąpiły błędy"
      render 'edit'
    end
  end

  def destroy
    @stop = Stop.find(params[:id])
    @stop.destroy

    redirect_to stops_path
  end

  def show
    @stop = Stop.find(params[:id])
  end

  def index
    @stop = Stop.all
  end


  private
  def stop_params
    params.require(:stop).permit(:city, :place)
  end
end
