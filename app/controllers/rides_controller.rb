class RidesController < ApplicationController
  before_action :authenticate_user!
  before_action :user_check_owner

  def new
    @ride = Ride.new
    @vehicles = Vehicle.all
    @routes = Route.all
    @weekDays = WeekDay.all
  end

  def edit
    @ride = Ride.find(params[:id])
    @vehicles = Vehicle.all
    @routes = Route.all
    @weekDays = WeekDay.all
  end

  def create
    @ride = Ride.new(ride_params)
    if @ride.save
      redirect_to @ride
    else
      flash[:alert] = "Wystąpiły błędy"
      render 'new'
    end
  end

  def update
    @ride = Ride.find(params[:id])

    if @ride.update(ride_params)
      redirect_to @ride
    else
      flash[:alert] = "Wystąpiły błędy"
      render 'edit'
    end
  end

  def destroy
    @ride = Ride.find(params[:id])
    @ride.destroy

    redirect_to rides_path
  end

  def show
    @ride = Ride.find(params[:id])
  end

  def index
    @ride = Ride.all
  end

  private
  def ride_params
    params.require(:ride).permit(:startTime, :route_id, :vehicle_id, :week_day_ids => [])
  end
end
