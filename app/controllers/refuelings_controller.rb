class RefuelingsController < ApplicationController
  before_action :authenticate_user!
  before_action :user_check

  def user_check
    unless current_user.try(:driver?) or current_user.try(:owner?)
      flash[:error] = "Nie posiadasz uprawnień do przeglądania tej strony"
      redirect_to root_path
    end
  end

  def new
    @refueling = Refueling.new
    @vehicles = Vehicle.all

  end

  def edit
    @refueling = Refueling.find(params[:id])
    @vehicles = Vehicle.all
  end

  def create
    @vehicles = Vehicle.all
    @refueling = Refueling.new(refuelings_params)
    if @refueling.save
      redirect_to @refueling
    else
      flash[:alert] = "Wystąpiły błędy"
      render 'new'
    end
  end

  def update

    @refueling = Refueling.find(params[:id])


    if @refueling.update(refuelings_params)
      redirect_to @refueling
    else
      flash[:alert] = "Wystąpiły błędy"
      render 'edit'
    end
  end

  def destroy
    @refueling = Refueling.find(params[:id])
    @refueling.destroy

    redirect_to refuelings_path
  end

  def index
    @refueling = Refueling.all
  end

  def show

    @refueling = Refueling.find(params[:id])
  end


  private
  def refuelings_params
    params.require(:refueling).permit(:vehicle_id, :capacity, :price, :date)

  end
end
