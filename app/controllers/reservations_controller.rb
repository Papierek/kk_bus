class ReservationsController < ApplicationController
  before_action :authenticate_user!

  def before_new
    @Stops = Stop.all
  end

  def payment
    if params[:reservation_id]
    else
      redirect_to reservations_path
    end
  end

  def payment_process
    @reservation = Reservation.find(params[:reservation_id])
    if current_user.try(:id) == @reservation.user.try(:id)
      @reservation.update(payment_status: true)
      @reservation.user.update(points: @reservation.user.points + @reservation.stops_price.try(:price).to_i * 5)
      redirect_to reservations_path
    else
      redirect_to reservations_path
    end
  end

  def new
    @reservation = Reservation.new
    @Stops = Stop.all

    begin
      @date = Date.new params[:date][:year].to_i, params[:date][:month].to_i, params[:date][:day].to_i
      if @date < DateTime.now.beginning_of_day or params[:date][:year].to_i > Time.now.year + 1 or params[:date][:year].to_i < Time.now.year
        flash[:alert] = "Podałeś złą datę"
        redirect_to action: "before_new"
        return
      end
      if @date.wday == 0
        day = 7
      else
        day = @date.wday
      end
      @rides = Ride.joins(:week_days).joins(:route => :order_of_stops).joins("INNER JOIN order_of_stops AS o2 ON o2.route_id = routes.id").
          where("week_days.id = ? and order_of_stops.stop_id = ? and o2.stop_id = ? and order_of_stops.order < o2.order", day, params[:startStop], params[:endStop])
      if @rides.count == 0
        flash[:alert] = "Niestety nie istnieją przejazdy o podanych parametrach"
        redirect_to action: "before_new"
        return
      end
    rescue
      flash[:alert] = "Wypełnij wszystkie pola formularza"
      redirect_to action: "before_new"
      return
    end

  end

  def create
    @reservation = Reservation.new(reservation_params)
    @rides = Ride.all
    if @reservation.driver_ride = get_driver_ride_from_ride_and_date
      if @reservation.save
        redirect_to @reservation
        return
      else
        flash[:alert] = "Wystąpiły błędy"
        redirect_to action: "new"
        return
      end
    else
      flash[:alert] = "Nie ma wolnych miejsc"
      redirect_to action: "new"
      return
    end

  end

  def destroy
    @reservation = Reservation.find(params[:id])
    if @reservation.payment_status
      @reservation.user.update(points: @reservation.user.points - @reservation.stops_price.try(:price).to_i * 5)
    end
    @reservation.destroy

    redirect_to reservations_path
  end

  def show
    @reservation = Reservation.find(params[:id])
  end

  def index
    unless current_user.try(:owner?)
      @reservation = Reservation.where(user: current_user)
    else
      @reservation = Reservation.all
    end
  end


  private
  def reservation_params
    params.require(:reservation).permit(:ride).merge(user_id: current_user.try(:id), stops_price: StopsPrice.where(startStop: params[:startStop], endStop: params[:endStop]).first)
  end

  def get_driver_ride_from_ride_and_date
    ride = Ride.find(params[:reservation][:ride])
    date = Date.new params[:date][:year].to_i, params[:date][:month].to_i, params[:date][:day].to_i

    if driver_ride = DriverRide.where(ride: ride, date: date).first
      if driver_ride.free_places > 0
        driver_ride.free_places = driver_ride.free_places - 1
        driver_ride
      else
        false
      end
    else
      if weekday_match? ride, date
        driver_ride = DriverRide.create(ride_id: ride.try(:id), date: date)
        driver_ride.free_places = driver_ride.free_places - 1
        driver_ride
      else
        false
      end
    end
  end

  def weekday_match?(ride,date)
    ride_days = ride.week_days.pluck(:id)

    if date.wday == 0
      day = 7
    else
      day = date.wday
    end

    ride_days.include?(day)
  end
end
