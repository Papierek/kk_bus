class RewardStatusesController < ApplicationController
  before_action :authenticate_user!
  before_action :user_check, only: [:edit, :update, :destroy]

  def user_check
    unless current_user.try(:owner?)
      flash[:error] = "Nie posiadasz uprawnień do przeglądania tej strony"
      redirect_to root_path
    end
  end

  def edit
    @reward_status = RewardStatus.find(params[:id])
  end

  def create
    @reward_status = RewardStatus.new(reward_status_params)
    reward = Reward.find(params[:reward_status][:reward_id])
    if current_user.points >= reward.price
      if @reward_status.save
        current_user.update(points: current_user.points - reward.price)
        redirect_to @reward_status
      else
        flash[:alert] = "Wystąpiły błędy"
        redirect_to rewards_list_path
      end
    else
      flash[:alert] = "Masz za mało punktów"
      redirect_to rewards_list_path
    end
  end

  def update
    @reward_status = RewardStatus.find(params[:id])

    if @reward_status.update(reward_status_params)
      redirect_to @reward_status
    else
      flash[:alert] = "Wystąpiły błędy"
      render 'edit'
    end
  end

  def destroy
    @reward_status = RewardStatus.find(params[:id])
    @reward_status.destroy

    redirect_to reward_statuses_path
  end

  def show
    @reward_status = RewardStatus.find(params[:id])
  end

  def index
    if current_user.try(:owner?)
      @reward_status = RewardStatus.all
    else
      @reward_status = RewardStatus.where(user: current_user)
    end
  end

  private
  def reward_status_params
    params.require(:reward_status).permit(:reward_id, :user_id, :status)
  end
end
