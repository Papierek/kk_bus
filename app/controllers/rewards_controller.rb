class RewardsController < ApplicationController
  before_action :authenticate_user!
  before_action :user_check_owner, only: [:index, :new, :create, :edit, :update, :destroy]

  def new
    @reward = Reward.new
  end

  def edit
    @reward = Reward.find(params[:id])
  end

  def create
    @reward = Reward.new(reward_params)
    if @reward.save
      if params[:reward][:avatar].blank?
        redirect_to @reward
      else
        render action: :crop
      end
    else
      flash[:alert] = "Wystąpiły błędy"
      render 'new'
    end
  end


  def update
    @reward = Reward.find(params[:id])

    if @reward.update(reward_params)
      if params[:reward][:avatar].blank?
        redirect_to @reward
      else
        render action: :crop
      end
    else
      flash[:alert] = "Wystąpiły błędy"
      render 'edit'
    end
  end

  def destroy
    @reward = Reward.find(params[:id])
    @reward.destroy

    redirect_to rewards_path
  end

  def show
    @reward = Reward.find(params[:id])
  end

  def index
    @rewards = Reward.all
  end

  def crop
    debugger
  end

  private

  def reward_params
    params.require(:reward).permit(:title, :description, :price, :avatar, :crop_x, :crop_y, :crop_w, :crop_h)
  end
end
