class SecretaryHoursController < ApplicationController
  before_action :authenticate_user!
  before_action :user_check
  before_action :user_check_owner, only: [:new, :create, :edit, :update, :destroy]

  def user_check
    unless current_user.try(:secretary?) or current_user.try(:owner?)
      flash[:error] = "Nie posiadasz uprawnień do przeglądania tej strony"
      redirect_to root_path
    end
  end

  def new
    @sh = SecretaryHours.new
    @weekDays = WeekDay.all
    @users = User.where(secretary: true)
  end

  def edit
    @sh = SecretaryHours.find(params[:id])
    @weekDays = WeekDay.all
    @users = User.where(secretary: true)
  end

  def create
    @sh = SecretaryHours.new(secretary_hours_params)
    if @sh.save
      redirect_to @sh
    else
      flash[:alert] = "Wystąpiły błędy"
      render 'new'
    end
  end

  def update
    @sh = SecretaryHours.find(params[:id])

    if @sh.update(secretary_hours_params)
      redirect_to secretary_hours_path
    else
      flash[:alert] = "Wystąpiły błędy"
      render 'edit'
    end
  end

  def destroy
    @sh = SecretaryHours.find(params[:id])
    @sh.destroy

    redirect_to secretary_hours_path
  end

  def show
    @sh = SecretaryHours.find(params[:id])
  end

  def index
    @sh = SecretaryHours.all
  end


  private
  def secretary_hours_params
    params.require(:secretary_hours).permit(:start, :end, :user_id, :week_day_ids => [])
  end
end
