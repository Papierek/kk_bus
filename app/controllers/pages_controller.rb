class PagesController < ApplicationController
  def index
  end

  def price_list
    @routes = Route.all
  end

  def rewards_list
    @rewards = Reward.all
  end
end
