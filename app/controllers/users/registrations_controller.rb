class Users::RegistrationsController < Devise::RegistrationsController
  prepend_before_filter :require_no_authentication, only: [:cancel]
  before_filter :authorize_admin, only: [:new, :create]

  def authorize_admin
    return if current_user.try(:owner?) or !user_signed_in?
    redirect_to root_path, alert: 'Nie jesteś administratorem!'
  end
end