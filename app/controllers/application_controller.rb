class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:firstName, :lastName, :email, :birthDate, :password, :points, :driver, :secretary) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:firstName, :lastName, :email, :birthDate, :password, :current_password, :points, :driver, :secretary) }
  end

  def user_check_owner
    unless current_user.try(:owner?)
      flash[:error] = "Nie posiadasz uprawnień do przeglądania tej strony"
      redirect_to root_path
    end
  end
end
