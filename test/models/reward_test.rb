require 'test_helper'

class RewardTest < ActiveSupport::TestCase
  test "should not save reward without title and description" do
    reward = Reward.new
    assert_not reward.save
  end
end
