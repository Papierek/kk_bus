require 'test_helper'

class StopTest < ActiveSupport::TestCase
  test "should not save stop without city and place" do
    stop = Stop.new
    assert_not stop.save
  end
end
