require 'test_helper'

class VehicleTest < ActiveSupport::TestCase
  test "should not save vehicle without brand and model" do
    vehicle = Vehicle.new
    assert_not vehicle.save
  end
end
