require 'test_helper'

class RefuelingTest < ActiveSupport::TestCase
  test "should not save refueling without price and capacity" do
    refueling = Refueling.new
    assert_not refueling.save
  end
end
